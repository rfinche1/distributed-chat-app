Instructions for Client App

By default the Client App looks for the server at 'localhost' on port 4225. If 
you wish to change the server address, inside the LoginWindowManager class, change
the value of the HOST variable to the address you wish to connect to.
