package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class Client {
  

  private Socket socket;
  private String userName;
  private InputStreamReader incomingMessages;
  private PrintStream outgoingMessages;
  private BufferedReader bufferedReader;

  /**
   * Constructor for a Client object.
   * 
   * @throws IOException
   * 
   * @postcondition A new Client is created.
   */
  public Client(String host, int port) throws IOException {
    this.socket = new Socket(host, port);
    this.incomingMessages = new InputStreamReader(this.socket.getInputStream());
    this.outgoingMessages = new PrintStream(this.socket.getOutputStream());
    this.bufferedReader = new BufferedReader(this.incomingMessages);
  }

  /**
   * Sends a message to the socket's PrintStream.
   * 
   * @param message the message being sent
   * 
   * @postcondition A messages is sent to the socket's PrintStream.
   */
  public void sendMessage(String message) {
    this.outgoingMessages.println(message);
  }

  /**
   * Gets a message from the socket's InputStreamReader.
   * 
   * @return a message from the socket's InputStreamReader.
   * 
   * @throws IOException if there's a problem with the server.
   */
  public String getMessage() throws IOException {
    return this.bufferedReader.readLine();
  }

  /**
   * Gets the InputStreamReader from the socket.
   * 
   * @return the InputStreamReader from the socket.
   */
  public InputStreamReader getIncomingMessages() {
    return this.incomingMessages;
  }

  /**
   * Gets the userName of the Client.
   * 
   * @return the userName of the Client.
   */
  public String getUserName() {
    return this.userName;
  }

  /**
   * Sets the userName of the Client.
   * 
   * @param userName the desired userName.
   */
  public void setUserName(String userName) {
    this.userName = userName;
  }

}