package application;

import controllers.LoginWindowController;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {
  private LoginWindowController controller;

  public Main() {
    this.controller = new LoginWindowController();
  }

  @Override
  public void start(Stage primaryStage) {
    this.controller.showStage();
  }

  public static void main(String[] args) {
    launch(args);
  }
}
