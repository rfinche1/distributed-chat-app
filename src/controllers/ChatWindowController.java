package controllers;

import java.io.IOException;
import java.util.ArrayList;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.Client;

public class ChatWindowController {
  private Stage stage;
  private Client client;
  private ObservableList<String> messages;
  private static final String SERVER_CHAT_HANDLE = "Server: ";
  private static final String USER_TAG = "USER:";
  private static final String UPDATE = "UPDATE_USERLISTS";
  private static final String OK = "OK";
  private static final String USER_LIST = "USER_LIST";
  private static final String USERS_END = "USERS_END";
  private static final String TERMINATE = "TERMINATE";

  @FXML
  private ListView<String> lstViewOutput;

  @FXML
  private TextField txtFieldMessage;

  @FXML
  private Button btnSend;

  @FXML
  private ListView<String> loggedInListView;

  @FXML
  void onSendClick(ActionEvent event) {
    String message = this.txtFieldMessage.getText();
    this.addMessage(message);
    this.txtFieldMessage.setText("");
  }
  
  @FXML
  void onEnter(KeyEvent event) {
    String message = this.txtFieldMessage.getText();
    this.addMessage(message);
    this.txtFieldMessage.setText("");
  }

  @FXML
  private void initialize() {
    this.lstViewOutput.setItems(messages);
    this.populateUserListView();
  }

  /**
   * Constructor for ChatWindowController.
   * 
   * @param client the client for the window
   * 
   * @postcondition A new ChatWindowController is created and checking for for messages is started.
   */
  public ChatWindowController(Client client) {
    this.client = client;
    this.stage = new Stage();
    this.messages = FXCollections.observableArrayList();

    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/ChatWindow.fxml"));
      loader.setController(this);
      stage.setScene(new Scene(loader.load()));
      stage.setTitle("Chat Window");
    } catch (IOException e) {
      e.printStackTrace();
    }

    this.checkForMessages(this.client);

    this.stage.setOnCloseRequest((EventHandler<WindowEvent>) new EventHandler<WindowEvent>() {

      @Override
      public void handle(WindowEvent t) {
        client.sendMessage(TERMINATE);
        Platform.exit();
        System.exit(0);
      }
    });
    
    txtFieldMessage.setOnKeyPressed((event) -> { 
      if (event.getCode() == KeyCode.ENTER) {
        this.onEnter(event);
      } 
    });
  }


  private void checkForMessages(Client client) {
    new Thread(new Runnable() {
      
      @Override
      public void run() {
        while (true) {
          String temp = "";
          try {
            temp = client.getMessage();
          } catch (IOException e1) {
            e1.printStackTrace();
          }
          final String message = temp;

          Platform.runLater(new Runnable() {
            @Override
            public void run() {
              if (isViewableMessage(message)) {
                messages.add(message);
              }
              if (message.equals(SERVER_CHAT_HANDLE + UPDATE)) {
                populateUserListView();
              }
            }

            private boolean isViewableMessage(final String message) {
              return !message.equals(OK)
                  && !message.equals(SERVER_CHAT_HANDLE + UPDATE)
                  && !message.contains(USER_TAG);
            }
          });
          try {
            Thread.sleep(10);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    }).start();
  }

  private void populateUserListView() {
    this.client.sendMessage(USER_LIST);
    String response = "";
    boolean exit = false;
    ArrayList<String> users = new ArrayList<String>();
    while (exit == false) {
      try {
        response = this.client.getMessage();
      } catch (IOException e) {
        e.printStackTrace();
      }
      if (!response.equals(USERS_END)) {
        if (response.contains(USER_TAG)) {
          users.add(response.replaceAll(USER_TAG, ""));
        }
      } else {
        exit = true;
      }
    }
    this.loggedInListView.setItems(FXCollections.observableArrayList(users));
  }

  /**
   * Shows the current Stage.
   * 
   * @postcondition The current Stage is shown.
   */
  public void showStage() {
    this.stage.show();
  }

  /**
   * Adds a message to the server.
   * 
   * @param message the message being added
   * 
   * @postcondition A message is added to the server.
   */
  public void addMessage(String message) {
    this.client.sendMessage(message);
    this.lstViewOutput.setItems(messages);
  }

  /**
   * Manually adds a message to the list of messages.
   * 
   * @param message the message being added
   * 
   * @postcondition A message is added to the server.
   */
  public void manuallyAddMessage(String message) {
    this.messages.add(message);
    this.lstViewOutput.setItems(messages);
  }
}
