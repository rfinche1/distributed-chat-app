package controllers;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import model.Client;

public class LoginWindowController {
  private Client client;
  private Stage stage;
  private static final String HOST = "localhost";
  private static final int PORT = 4225; // #convention for CS 4225  
  
  @FXML
  private TextField txtFieldUserName;

  @FXML
  private Button btnLogin;
  
  @FXML
  private Label errorLabel;
    
  @FXML
  void login(ActionEvent event) {
    this.loadChatWindow();
  }
  
  /**
   * Constructor for a LoginWindowController.
   * 
   * @postcondition A new LoginWindowController is created.
   */
  public LoginWindowController() {
    this.stage = new Stage();
    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/LoginWindow.fxml"));
      loader.setController(this);
      stage.setScene(new Scene(loader.load()));
      stage.setTitle("Chat Login");
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    txtFieldUserName.setOnKeyPressed((event) -> { 
      if (event.getCode() == KeyCode.ENTER) {
        this.loadChatWindow(); 
      } 
    });
  }
    
  public void showStage() {
    this.stage.show();
  }

  private void loadChatWindow() {
    this.client = null;
    String response = null;
    try {
      var userName = this.txtFieldUserName.getText();
      if (userName.isEmpty()) {
        this.errorLabel.setText("User name can not be blank!");
        this.errorLabel.setLayoutX(145);
        return;
      }
      this.client = new Client(HOST, PORT);
      this.client.setUserName(userName);
      
      response = this.client.getMessage();
      
      if (response.equals("REQUEST_USERNAME")) {
        this.client.sendMessage(this.client.getUserName());
      } 
      
      response = this.client.getMessage();
      
      if (response.equals("USER_EXISTS")) {
        this.errorLabel.setText("Username taken!");
        this.errorLabel.setLayoutX(175);
        this.txtFieldUserName.setText("");
        this.stage.show();
        return;
      } 
      
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    ChatWindowController controller = new ChatWindowController(this.client);
    
    controller.manuallyAddMessage(response);
    controller.showStage();
    this.stage.close();
  }
}

